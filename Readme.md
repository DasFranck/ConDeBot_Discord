![ConDeBot](https://dasfranck.fr/Stuff/ConDeBot-Banner.png)

# ConDeBot (Discord Version)
Un con de bot Discord.

## Features
- Fully Asynchrounous Bot written in python 3.6
- Operator system
- Dynamic plugin system
- A lot of plugins included (and more to come!)

## Plugins
- **Coffee**  
Serve a delicous coffee (or tea) with a random phrase
- **Kaamelott**  
Display random quotes and specific quotes from the french TV series Kaamelott
- **List**  
Kinda like the replier module but with list and random picking
- **Opmod**  
Manage Operators
- **Replier**  
Register custom replies for the bot. You can send them, lock them and display stats about them.
- **Source**  
Display links to his own source code
- **Status**  
Change the bot status and his played game on discord
- **Suicide**  
Remotly kill the bot
- **Version**  
Display the bot version

## Dependencies
* [discord.py](https://github.com/Rapptz/discord.py)
* [hjson](https://github.com/hjson/hjson-py)

## Usage
*To be writed soon.*

## Changelog
### Version 1.0.1 (16/03/2017)
* Quick hotfix

### Version 1.0 (06/02/2017)
* Initial Release
