#!/usr/bin/env python3
# -*- coding: utf-8 -*-

NAME = "ConDeBot"
SHORT_NAME = "CDB"
DESCRIPTION = "ConDeBot - Un con de bot Discord"
CMD_PREFIX = ""

OPS_FILE_PATH = "jsonfiles/"
OPS_FILE = OPS_FILE_PATH + "ops.json"
REPLIES_FILE_DIR = "jsonfiles/replies/"
LISTS_FILE_DIR = "jsonfiles/lists/"

token = ""
